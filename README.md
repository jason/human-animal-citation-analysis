# Human-animal-citation-analysis

A citation analysis designed to ask whether neuroscience papers with human subjects are more likely to cite human than animal papers, and vice-versa.

The code in this reposity was used to generate Figure 1 of the recently submitted paper titled "Combining human and animal research: why is it so difficult and is it worth it?" by Jason Lerch and Matthew Rushworth.

The code is organized into two files:

- the functions that do the work are in "citation-analysis-funcs.R"
- the workbook that generates the figure is "refs-workbook-updated.Rmd"

In addition, all the files needed are in the data subdirectory.